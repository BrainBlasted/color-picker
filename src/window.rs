// window.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use libadwaita::subclass::prelude::*;

use ashpd::desktop::screenshot;
use ashpd::{zbus, WindowIdentifier};
use gtk_macros::action;

use once_cell::sync::Lazy;
use std::cell::RefCell;

use crate::color_box::ColPickerColorBox;
use crate::color_popover::ColPickerColorPopover;
use crate::recent_box::ColPickerRecentBox;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/ColorPicker/ui/window.ui")]
    pub struct ColPickerWindow {
        #[template_child]
        pub color_box: TemplateChild<ColPickerColorBox>,
        #[template_child]
        pub hex_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub recent_box: TemplateChild<ColPickerRecentBox>,
        #[template_child]
        pub popover: TemplateChild<ColPickerColorPopover>,

        pub hex: RefCell<String>,
        pub settings: gio::Settings,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColPickerWindow {
        const NAME: &'static str = "ColPickerWindow";
        type Type = super::ColPickerWindow;
        type ParentType = libadwaita::ApplicationWindow;

        fn new() -> Self {
            Self {
                color_box: TemplateChild::default(),
                hex_entry: TemplateChild::default(),
                recent_box: TemplateChild::default(),
                popover: TemplateChild::default(),
                hex: RefCell::new(String::from("#FFFFFF")),
                settings: gio::Settings::new("org.gnome.design.ColorPicker"),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ColPickerWindow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_string(
                    "hex",
                    "Hex",
                    "The current hex code selected",
                    Some("#FFFFFF"), // Default value
                    glib::ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.setup_actions();

            self.hex_entry
                .connect_icon_press(clone!(@weak obj => move |_, pos| {
                    if pos == gtk::EntryIconPosition::Secondary {
                        obj.copy_hex();
                    }
                }));

            // Reload state
            let hex = self.settings.string("current-color");
            obj.set_hex(hex.to_string());

            let colors: Vec<String> = self
                .settings
                .strv("recent-colors")
                .iter()
                .rev()
                .map(|c| c.to_string())
                .collect();
            for color in colors {
                self.recent_box.push_color(&color);
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "hex" => self.hex.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "hex" => obj.set_hex(value.get::<String>().unwrap()),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for ColPickerWindow {}
    impl WindowImpl for ColPickerWindow {
        fn close_request(&self, _window: &Self::Type) -> glib::signal::Inhibit {
            self.settings
                .set_string("current-color", &self.hex.borrow())
                .unwrap();
            let colors = self.recent_box.colors();
            let colors: Vec<_> = colors.iter().map(String::as_str).collect();
            self.settings.set_strv("recent-colors", &colors).unwrap();
            glib::signal::Inhibit(false)
        }
    }
    impl ApplicationWindowImpl for ColPickerWindow {}
    impl AdwApplicationWindowImpl for ColPickerWindow {}
}

glib::wrapper! {
    pub struct ColPickerWindow(ObjectSubclass<imp::ColPickerWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl ColPickerWindow {
    pub fn new<A: glib::IsA<gtk::Application>>(app: &A) -> Self {
        glib::Object::new(&[("application", app)]).unwrap()
    }

    pub fn setup_actions(&self) {
        action!(
            self,
            "select-color",
            clone!(@weak self as win => move |_, _| {
                if let Err(e) = win.select_color() {
                    println!("{}", e);
                }
            })
        );
    }

    pub fn copy_hex(&self) {
        let imp = imp::ColPickerWindow::from_instance(self);
        let clipboard = self.clipboard();
        if imp.hex_entry.text_length() > 0 {
            clipboard.set_text(&imp.hex_entry.text().to_string());
        }
    }

    fn select_color(&self) -> zbus::fdo::Result<()> {
        let ctx = glib::MainContext::default();
        let root = self.root().unwrap();
        ctx.spawn_local(clone!(@weak self as win => async move {
            if let Ok(color) = pick_color(WindowIdentifier::from_native(&root).await).await {
                let rgb = colorsys::Rgb::new(color.red() * 255f64, color.green() * 255f64, color.blue() * 255f64, None);
                let hex = rgb.to_hex_string().to_uppercase();
                win.set_hex(hex);
            }
        }));

        Ok(())
    }

    fn set_hex(&self, hex: String) {
        if colorsys::Rgb::from_hex_str(&hex).is_ok() {
            let imp = imp::ColPickerWindow::from_instance(self);
            if hex != *imp.hex.borrow() {
                let old_hex = imp.hex.replace(hex);
                // If the window is visible, push the hex code to the recents box
                if self.is_visible() {
                    imp.recent_box.push_color(&old_hex);
                }
                self.notify("hex");
            }
        }
    }
}

async fn pick_color(window: WindowIdentifier) -> Result<screenshot::Color, ashpd::Error> {
    let connection = zbus::azync::Connection::session().await?;
    let proxy = screenshot::ScreenshotProxy::new(&connection).await?;
    let color = proxy.pick_color(&window).await?;
    Ok(color)
}
