// color_popover.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::subclass::prelude::*;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/ColorPicker/ui/color_popover.ui")]
    pub struct ColPickerColorPopover {
        #[template_child]
        pub color_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub hue_spin: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub saturation_spin: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub lightness_spin: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub red_spin: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub green_spin: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub blue_spin: TemplateChild<gtk::SpinButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColPickerColorPopover {
        const NAME: &'static str = "ColPickerColorPopover";
        type Type = super::ColPickerColorPopover;
        type ParentType = gtk::Popover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ColPickerColorPopover {}
    impl WidgetImpl for ColPickerColorPopover {}
    impl PopoverImpl for ColPickerColorPopover {}
}

glib::wrapper! {
    pub struct ColPickerColorPopover(ObjectSubclass<imp::ColPickerColorPopover>)
        @extends gtk::Widget, gtk::Popover;
}
