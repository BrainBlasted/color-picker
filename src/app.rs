// app.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later
#![allow(clippy::new_without_default)]

use glib::clone;
use glib::WeakRef;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use libadwaita::subclass::prelude::*;

use gtk_macros::action;

use std::cell::RefCell;

use crate::config;
use crate::window::ColPickerWindow;

mod imp {
    use super::*;

    #[derive(Default, Debug)]
    pub struct ColPickerApp {
        pub window: RefCell<Option<WeakRef<ColPickerWindow>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColPickerApp {
        const NAME: &'static str = "ColPickerApp";
        type Type = super::ColPickerApp;
        type ParentType = libadwaita::Application;
    }

    impl ObjectImpl for ColPickerApp {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.setup_actions();
        }
    }

    impl ApplicationImpl for ColPickerApp {
        fn activate(&self, application: &Self::Type) {
            self.parent_activate(application);
            if let Some(ref window) = *self.window.borrow() {
                window.upgrade().unwrap().present();
                return;
            }

            let window = ColPickerWindow::new(application);
            window.show();
            self.window.replace(Some(window.downgrade()));
        }
    }

    impl GtkApplicationImpl for ColPickerApp {}
    impl AdwApplicationImpl for ColPickerApp {}
}

glib::wrapper! {
    pub struct ColPickerApp(ObjectSubclass<imp::ColPickerApp>)
        @extends gio::Application, gtk::Application, libadwaita::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl ColPickerApp {
    pub fn new() -> Self {
        glib::Object::new(&[(
            "application-id",
            &String::from("org.gnome.design.ColorPicker"),
        )])
        .unwrap()
    }

    pub fn setup_actions(&self) {
        action!(
            self,
            "about",
            clone!(@weak self as app => move |_,_| {
                app.show_about();
            })
        );
    }

    fn show_about(&self) {
        let win = self.active_window();
        let dialog = gtk::AboutDialogBuilder::new()
            .authors(vec![
                "Christopher Davis <christopherdavis@gnome.org>".to_string()
            ])
            .artists(vec![
                "Laurent Baumann".to_string(),
                "Tobias Bernard".to_string(),
                "Sam Hewitt".to_string(),
            ])
            .license_type(gtk::License::Gpl30)
            .logo_icon_name("org.gnome.design.ColorPicker")
            .program_name("Color Picker")
            .version(config::VERSION)
            .modal(true)
            .build();

        dialog.add_credit_section(
            /*&i18n(*/ "Original app by", /*)*/
            &["Jente Hidskes"],
        );

        dialog.set_transient_for(win.as_ref());
        dialog.show()
    }
}
