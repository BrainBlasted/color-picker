// color_box.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/ColorPicker/ui/color_box.ui")]
    pub struct ColPickerColorBox {
        pub hex: RefCell<String>,
        pub provider: gtk::CssProvider,

        #[template_child]
        pub start_controls: TemplateChild<gtk::WindowControls>,
        #[template_child]
        pub end_controls: TemplateChild<gtk::WindowControls>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColPickerColorBox {
        const NAME: &'static str = "ColPickerColorBox";
        type Type = super::ColPickerColorBox;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.set_css_name("colorbox");
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ColPickerColorBox {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.style_context()
                .add_provider(&self.provider, gtk::STYLE_PROVIDER_PRIORITY_USER);
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_string(
                    "hex",
                    "Hex",
                    "The current hex code selected",
                    Some("#FFFFFF"), // Default value
                    glib::ParamFlags::READWRITE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "hex" => self.hex.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "hex" => obj.set_hex(value.get::<String>().unwrap()),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for ColPickerColorBox {}
    impl BoxImpl for ColPickerColorBox {}
}

glib::wrapper! {
    pub struct ColPickerColorBox(ObjectSubclass<imp::ColPickerColorBox>)
        @extends gtk::Widget, gtk::Box;
}

impl ColPickerColorBox {
    fn set_hex(&self, hex: String) {
        let imp = imp::ColPickerColorBox::from_instance(self);

        let css_class = crate::utils::icon_class_from_hex(&hex);
        imp.start_controls.set_css_classes(&[&css_class, "start"]);
        imp.end_controls.set_css_classes(&[&css_class, "end"]);

        imp.hex.replace(hex);
        imp.provider.load_from_data(
            &format!("@define-color color_picked {};", imp.hex.borrow()).as_bytes(),
        );
    }
}
