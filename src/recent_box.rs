// recent_box.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

use std::cell::RefCell;

use crate::color_button::ColPickerColorButton;

mod imp {
    use super::*;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/ColorPicker/ui/recent_box.ui")]
    pub struct ColPickerRecentBox {
        #[template_child]
        pub color_list: TemplateChild<gtk::Box>,

        pub latest_color: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColPickerRecentBox {
        const NAME: &'static str = "ColPickerRecentBox";
        type Type = super::ColPickerRecentBox;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            ColPickerColorButton::static_type();
            obj.init_template();
        }
    }

    impl ObjectImpl for ColPickerRecentBox {}
    impl WidgetImpl for ColPickerRecentBox {}
    impl BoxImpl for ColPickerRecentBox {}
}

glib::wrapper! {
    pub struct ColPickerRecentBox(ObjectSubclass<imp::ColPickerRecentBox>)
        @extends gtk::Widget, gtk::Box;
}

impl ColPickerRecentBox {
    pub fn push_color(&self, hex: &str) {
        let imp = imp::ColPickerRecentBox::from_instance(self);
        *imp.latest_color.borrow_mut() = hex.to_string();
        if let Some(mut child) = imp
            .color_list
            .first_child()
            .map(|c| c.downcast::<ColPickerColorButton>().unwrap())
        {
            let mut last_color = child.hex();
            child.set_hex(imp.latest_color.borrow().clone());
            while let Some(sibling) = child
                .next_sibling()
                .map(|c| c.downcast::<ColPickerColorButton>().unwrap())
            {
                child = sibling;
                let l = child.hex();
                child.set_hex(last_color.to_string());
                last_color = l;
            }
        }
    }

    pub fn colors(&self) -> Vec<String> {
        let imp = imp::ColPickerRecentBox::from_instance(self);
        let mut colors = Vec::new();
        if let Some(mut child) = imp
            .color_list
            .first_child()
            .map(|c| c.downcast::<ColPickerColorButton>().unwrap())
        {
            colors.push(child.hex());
            while let Some(sibling) = child
                .next_sibling()
                .map(|c| c.downcast::<ColPickerColorButton>().unwrap())
            {
                child = sibling;
                colors.push(child.hex());
            }
        }
        colors
    }
}
