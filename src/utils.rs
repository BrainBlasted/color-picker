// utils.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use colorsys::Rgb;

pub fn icon_class_from_hex(hex: &str) -> String {
    if let Ok(color) = Rgb::from_hex_str(hex) {
        let white = colorsys::Rgb::new(255.0, 255.0, 255.0, None);
        let black = colorsys::Rgb::new(0.0, 0.0, 0.0, None);

        let black_ratio = contrast_ratio(&black, &color);
        let white_ratio = contrast_ratio(&white, &color);

        if black_ratio > white_ratio {
            "dark".to_string()
        } else {
            "light".to_string()
        }
    } else {
        "dark".to_string()
    }
}

fn contrast_ratio(rgb1: &Rgb, rgb2: &Rgb) -> f64 {
    let l1 = luminance_ratio(&rgb1) + 0.05f64;
    let l2 = luminance_ratio(&rgb2) + 0.05f64;
    l1.max(l2) / l1.min(l2)
}

fn luminance_ratio(rgb: &Rgb) -> f64 {
    let rgb_vec = rgb
        .iter()
        .map(|col| {
            let col = col / 255f64;
            if col <= 0.3928f64 {
                col / 12.92f64
            } else {
                ((col + 0.055f64) / 1.055f64).powf(2.4f64)
            }
        })
        .collect::<Vec<f64>>();

    rgb_vec[0] * 0.2126f64 + rgb_vec[1] * 0.7152f64 + rgb_vec[2] * 0.0722f64
}
