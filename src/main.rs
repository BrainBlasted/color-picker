use gettextrs::*;
use gio::prelude::*;

mod app;
mod color_box;
mod color_button;
mod color_popover;
mod config;
mod recent_box;
mod utils;
mod window;
use crate::app::ColPickerApp;

fn main() {
    gtk::init().expect("Could not initialize GTK");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/color-picker.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("color-picker", config::LOCALEDIR);
    textdomain("color-picker");

    let app = ColPickerApp::new();
    let ret = app.run();
    std::process::exit(ret);
}
