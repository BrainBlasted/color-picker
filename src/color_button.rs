// color_button.rs
//
// Copyright 2021 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;

use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/ColorPicker/ui/color_button.ui")]
    pub struct ColPickerColorButton {
        pub provider: gtk::CssProvider,
        pub hex: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColPickerColorButton {
        const NAME: &'static str = "ColPickerColorButton";
        type Type = super::ColPickerColorButton;
        type ParentType = gtk::Button;

        fn class_init(klass: &mut Self::Class) {
            klass.set_css_name("colorbutton");
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ColPickerColorButton {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.style_context()
                .add_provider(&self.provider, gtk::STYLE_PROVIDER_PRIORITY_USER);
        }
    }

    impl WidgetImpl for ColPickerColorButton {}
    impl ButtonImpl for ColPickerColorButton {
        fn clicked(&self, button: &Self::Type) {
            let clipboard = button.clipboard();
            clipboard.set_text(&self.hex.borrow());
        }
    }
}

glib::wrapper! {
    pub struct ColPickerColorButton(ObjectSubclass<imp::ColPickerColorButton>) @extends gtk::Widget, gtk::Button;
}

impl ColPickerColorButton {
    pub fn set_hex(&self, hex: String) {
        let imp = imp::ColPickerColorButton::from_instance(self);

        let css_class = crate::utils::icon_class_from_hex(&hex);
        self.set_css_classes(&[&css_class]);

        imp.hex.replace(hex);
        self.set_tooltip_text(Some(&imp.hex.borrow()));
        imp.provider
            .load_from_data(&format!("@define-color color_picked {};", imp.hex.borrow()).as_bytes());
    }

    pub fn hex(&self) -> String {
        let imp = imp::ColPickerColorButton::from_instance(self);
        imp.hex.borrow().clone()
    }
}
